import '../../assets/css/style.css';
import'../../assets/css/bootstrap.min.css';
import'../../assets/css/font-awesome.min.css';
import'../../assets/css/responsive.css';
import'../../assets/css/hover-min.css';
import'../../assets/css/magnific-popup.css';
import React from 'react';
import project1 from "../../assets/images/project/project1.jpg"
import project2 from "../../assets/images/project/project2.jpg"
import project3 from "../../assets/images/project/project3.jpg"
import project4 from "../../assets/images/project/project4.jpg"
import project5 from "../../assets/images/project/project5.jpg"

const Project=()=>
{
return(
<section id="project"  className="project">
    <div className="container">
        <div className="project-details">
            <div className="project-header text-left">
                <h2>Our Finished Projects</h2>
                <p>
                    Pallamco laboris nisi ut aliquip ex ea commodo consequat. 
                </p>
            </div>
            <div className="project-content">
                <div className="gallery-content">
                    <div className="isotope">
                        <div className="row">
                            <div className=" col-md-4 col-sm-12">
                                <div className="item big-height">
                                    <img src={project1} alt="portfolio image"/>
                                    <div className="isotope-overlay">
                                        <a href="project.html">
                                            <span className="lnr lnr-link"></span>
                                            
                                        </a>
                                        <h3>
                                            <a href="project.html">
                                                aquisition plan
                                            </a>
                                        </h3>
                                        <p>busisness planning</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 col-sm-12">
                                <div className="row">
                                    <div className="col-sm-6 col-xs-12">
                                        <div className="item">
                                            <img src={project2} alt="portfolio image"/>
                                            <div className="isotope-overlay">
                                                <a href="project.html">
                                                    <span className="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        aquisition plan
                                                    </a>
                                                </h3>
                                                <p>busisness planning</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-xs-12">
                                        <div className="item">
                                            <img src={project3} alt="portfolio image"/>
                                            <div className="isotope-overlay">
                                                <a href="project.html">
                                                    <span className="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        aquisition plan
                                                    </a>
                                                </h3>
                                                <p>busisness planning</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 col-xs-12">
                                        <div className="item">
                                            <img src={project4} alt="portfolio image"/>
                                            <div className="isotope-overlay">
                                                <a href="project.html">
                                                    <span className="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        aquisition plan
                                                    </a>
                                                </h3>
                                                <p>busisness planning
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-xs-12">
                                        <div className="item">
                                            <img src={project5} alt="portfolio image"/>
                                            <div className="isotope-overlay">
                                                <a href="project.html">
                                                    <span className="lnr lnr-link"></span>
                                                    
                                                </a>
                                                <h3>
                                                    <a href="project.html">
                                                        aquisition plan
                                                    </a>
                                                </h3>
                                                <p>busisness planning
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div className="project-btn text-center">
            <a href="project.html"  className="project-view">view all
            </a>
        </div>
    </div>

</section>
)
}
export default Project;