import React from "react"
import '../../assets/css/style.css';
import'../../assets/css/bootstrap.min.css';
import'../../assets/css/font-awesome.min.css';
import'../../assets/css/responsive.css';
// import'../../assets/css/owl.carousel.min.css';
import img1 from "../../assets/images/client/client1.png"
import img2 from "../../assets/images/client/client2.png"
import img3 from "../../assets/images/client/client3.png"
import img4 from "../../assets/images/client/client4.png"
import img5 from "../../assets/images/client/client5.png"
const Client=()=>
{
    return(
        <section className="clients">
        <div className="container">
            <div className="clients-area">
                <div className="owl-carousel owl-theme" style={{display:'flex',flexWrap:'wrap',marginLeft:'12%'}} id="client">
                    <div className="item">
                        <a href="#">
                            <img src={img1} alt="brand-image" />
                        </a>
                    </div>
                    <div className="item">
                        <a href="#">
                            <img src={img2} alt="brand-image" />
                        </a>
                    </div>
                    <div className="item">
                        <a href="#">
                            <img src={img3} alt="brand-image" />
                        </a>
                    </div>
                    <div className="item">
                        <a href="#">
                            <img src={img4} alt="brand-image" />
                        </a>
                    </div>
                    <div className="item">
                        <a href="#">
                            <img src={img5} alt="brand-image" />
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    )
}
export default Client;