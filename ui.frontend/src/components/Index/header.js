import "../../assets/css/style.css";
import "../../assets/css/bootstrap.min.css";
import "../../assets/css/bootsnav.css";
import Img1 from "../../assets/images/logo/logo.png"
import React from 'react';



const Header = () => {

    return (
        <div style={{ display: "block", height: "105px", backgroundColor: 'rgba(115,112,216,1)',/* width: auto, *//* margin: auto, */position: "relative", float: 'none', inset: 'auto', verticalAlign: 'top' }}>
            <section id="menu" >
                <div className="container">
                    <div>
                        <nav className="navbar navbar-default" style={{ backgroundColor: 'rgba(115,112,216,1)', borderColor: 'rgba(115,112,216,1)' }}>

                            <div className="navbar-header">
                                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <a className="navbar-brand">
                                    <img src={Img1} alt="logo" />
                                </a>
                            </div>


                            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul className="nav navbar-nav navbar-right">
                                    <li><a style={{ color: 'white' }}>Home</a></li>
                                    <li><a style={{ color: 'white' }} >About</a></li>
                                    <li><a style={{ color: 'white' }} >Service</a></li>
                                    <li><a style={{ color: 'white' }} >Project</a></li>
                                    <li><a style={{ color: 'white' }} >Team</a></li>
                                    <li><a style={{ color: 'white' }} >Blog</a></li>
                                    <li><a style={{ color: 'white' }} >Contact</a></li>
                                    <li >
                                        <a href="#" >
                                            <span className="lnr lnr-cart"></span>
                                        </a>
                                    </li>
                                    <li className="search">
                                        <form action="">
                                            <input type="text" name="search" placeholder="Search...." />
                                            <a href="#">
                                                <span className="lnr lnr-magnifier"></span>
                                            </a>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>

            </section>
        </div>
    )
}
export default Header