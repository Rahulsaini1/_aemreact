import React from "react";
import "../../assets/css/style.css"
import "../../assets/css/bootstrap.min.css"
import img1 from "../../assets/images/counter/counter1.png"
import img2 from "../../assets/images/counter/counter2.png"
import img3 from "../../assets/images/counter/counter3.png"
import img4 from "../../assets/images/counter/counter4.png"

const Stats=()=>
{
return(
        <section  className="statistics">
    <div className="container">
        <div className="statistics-counter "> 
            <div className="col-md-3 col-sm-6">
                <div className="single-ststistics-box">
                    <div className="statistics-img">
                        <img src={img1} alt="counter-icon" />
                    </div>
                    <div className="statistics-content">
                        <div className="counter">2556</div>
                        <h3>days worked</h3>
                    </div>
                </div>
            </div>
            <div className="col-md-3 col-sm-6">
                <div className="single-ststistics-box">
                    <div className="statistics-img">
                        <img src={img2} alt="counter-icon" />
                    </div>
                    <div className="statistics-content">
                        <div className="counter">326</div>
                        <h3>project finished</h3>
                    </div>
                </div>
            </div>
            <div className="col-md-3 col-sm-6">
                <div className="single-ststistics-box">
                    <div className="statistics-img">
                        <img src={img3} alt="counter-icon" />
                    </div>
                    <div className="statistics-content">
                        <div className="counter">1526</div>
                        <h3>coffee cup</h3>
                    </div>
                </div>
            </div>
            <div className="col-md-3 col-sm-6">
                <div className="single-ststistics-box">
                    <div className="statistics-img">
                        <img src={img4} alt="counter-icon" />
                    </div>
                    <div className="statistics-content">
                        <div className="counter">856</div>
                        <h3>client satisfied</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    
    )
}
export default Stats;