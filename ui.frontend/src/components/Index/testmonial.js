import React from 'react';
import "../../assets/css/style.css"
import  test from '../../assets/images/client/testimonial1.jpg'
import  testimonial2 from '../../assets/images/client/testimonial2.jpg'
import  testimonial1 from '../../assets/images/client/testimonial1.jpg'
const TestMonial=()=>
{
return(
<section   className="testemonial">
    <div className="container">
        <div className="section-header text-center">
            <h2>
                <span>
                    what our client say about us
                </span>
            </h2>
        </div>
        <div className="owl-carousel owl-theme" id="testemonial-carousel">
            <div className="home1-testm item">
                <div className="home1-testm-single text-center">
                    <div className="home1-testm-img">
                        <img src={test} alt="img"/>
                    </div>
                    <div className="home1-testm-txt">
                        {/* <span className="icon section-icon">
                            <i className="fa fa-quote-left" aria-hidden="true"></i>
                        </span> */}
                        <p>
                            Lorem ipsum dolor sit amet conse adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam..
                        </p>
                        <h3>
                            <a href="#">
                                kevin watson
                            </a>
                        </h3>
                        <h4>CEO, Kingston</h4>
                    </div>
                </div>
            </div>
            <div className="home1-testm item">
                <div className="home1-testm-single text-center">
                    <div className="home1-testm-img">
                        <img src={testimonial2} alt="img"/>
                    </div>
                    <div className="home1-testm-txt">
                        {/* <span className="icon section-icon">
                            <i className="fa fa-quote-left" aria-hidden="true"></i>
                        </span> */}
                        <p>
                            Lorem ipsum dolor sit amet conse adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam..
                        </p>
                        <h3>
                            <a href="#">
                                kevin watson
                            </a>
                        </h3>
                        <h4>CEO, Kingston</h4>
                    </div>	
                </div>
            </div>
            <div className="home1-testm item">
                <div className="home1-testm-single text-center">
                    <div className="home1-testm-img">
                        <img src={testimonial1} alt="img"/>
                    </div>
                    <div className="home1-testm-txt">
                        {/* <span className="icon section-icon">
                            <i className="fa fa-quote-left" aria-hidden="true"></i>
                        </span> */}
                        <p>
                            Lorem ipsum dolor sit amet conse adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam..
                        </p>
                        <h3>
                            <a href="#">
                                kevin watson
                            </a>
                        </h3>
                        <h4>CEO, Kingston</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
    )
}
export default TestMonial