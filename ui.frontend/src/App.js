import { Page, withModel } from '@adobe/aem-react-editable-components';
import React from 'react';
import FHeader from './components/Index/FHeader';
import Header from './components/Index/header';
import WeDoStart from './components/Index/wedostart';
import Slider from './components/Index/slider';
import About_Us from './components/Index/about_us';
import Service from './components/Index/service';
import Stats from './components/Index/stats';
import Project from './components/Index/project'
import Team from './components/Index/team'
import Pricing from './components/Index/Pricing';
import TestMonial from './components/Index/testmonial';
import Client from './components/Index/client';
import News from './components/Index/news';
import Contact from './components/Index/contact';
import New_project from './components/Index/new_Project';
import Footer from './components/Index/footer';
import "../src/assets/css/style.css"


// This component is the application entry point
class App extends Page {
  render() {
    return (
      <div>
        <FHeader />
        <Header />
        <Slider />
        <WeDoStart />
        <About_Us />
        <Service />
        <Stats />
        <Project />
        {/* <Team /> */}
        <Pricing />
        <TestMonial />
        <Client />
        <News />
        <Contact />
        <New_project />
        <Footer />
        {/* {this.childComponents}
        {this.childPages} */}
      </div>
    );
  }
}

export default withModel(App);
